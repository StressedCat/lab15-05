#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random
from Auto import CarDef
from Cambios import Actualizar
from Ruedas import Desgastar
from os import system, name


def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')


def Velocidad(Vel, Gas):
    comprobar = False
    while comprobar is False:
            print("Presione W para aumentar su velocidad a 20 km/s")
            print("Presione S para disminuir su velocidad a 20 km/s")
            choice = input(">")
            print(Auto.vel)
            if choice.upper() == "W":
                if Auto.vel != 120:
                    km = Actualizar(Vel + 20, Gas)
                    Auto.VelocidadCambio(km)
                comprobar = True
            elif choice.upper() == "S":
                if Auto.vel != 0:
                    km = Actualizar(Vel - 20, Gas)
                    Auto.VelocidadCambio(km)
                comprobar = True
            else:
                print("eleccion incorrecta")


def Ruedas(R1, R2, R3, R4):
    """
        Para bajar la integridad de las ruedas, se hara un random entre 1-10
        en donde le bajara estos puntos a la integridad actual de la rueda.

        Se haran en diferentes x para evitar que todos tengan el mismo desgaste
    """
    x = 0
    while x != 10:
        desgaste = random.randint(1, 10)
        if x == 0:
            DR1 = R1 - desgaste
        elif x == 1:
            DR2 = R2 - desgaste
        elif x == 2:
            DR3 = R3 - desgaste
        elif x == 3:
            DR4 = R4 - desgaste
        elif x == 4:
            DT = Desgastar(DR1, DR2, DR3, DR4)
            Auto.DesgasteRuedas(DT, DT, DT, DT)
        x = x + 1


if __name__ == '__main__':
    vm = 120
    power = False
    motor = random.randint(0, 1)
    """
        Para obtener el motor del vehiculo se hara un random entre 0 y 1, en el
        caso que sea 1, sera un motor 1.6, caso contrario, motor 1.2
    """
    if motor == 0:
        motor = 1.2
        div = 20
    else:
        motor = 1.6
        div = 14
    km = vel = 0
    tiempo = 0
    gas = r4 = r3 = r2 = r1 = 100
    # Se crea el Auto
    Auto = CarDef(motor, gas, vel, r1, r2, r3, r4)
    while Auto.gas > 0:
        """
            Mientras el gas sea mayor a 0, el auto seguira funcionando, la
            unica forma que termine es por medio del gas, el auto estará
            apagado cada vez que inicias el programa, tragando 1 litro
            de gasolina para iniciarlo, en el caso que las ruedas fallen
            el motor se apagará y se tendra que iniciar de nuevo, siempre
            se mostrara el estado del vehiculo
        """
        clear()
        Renovar = False
        if Auto.r1 <= 10:
            Ren = Desgastar(100, Auto.r2, Auto.r3, Auto.r4)
            Renovar = True
        if Auto.r2 <= 10:
            Ren = Desgastar(Auto.r1, 100, Auto.r3, Auto.r4)
            Renovar = True
        if Auto.r3 <= 10:
            Ren = Desgastar(Auto.r1, Auto.r2, 100, Auto.r4)
            Renovar = True
        if Auto.r4 <= 10:
            Ren = Desgastar(Auto.r1, Auto.r2, Auto.r3, 100)
            Renovar = True
        if Renovar is True:
            Auto.DesgasteRuedas(Ren, Ren, Ren, Ren)
            print("Una o mas ruedas del auto se desgastaron")
            print("Se para el motor y se cambia la rueda")
            power = False
        if power is True:
            print("\nSe avanzó por {} s a {} km/s".format(tiempo,
                                                            Auto.vel))
            print("\nTipo de motor: {}".format(motor))
            print("\nkm recorrido: {} km\nVelocidad: {} km/s".format(km,
                                                                    Auto.vel))
            print("\nGasolina: {} L".format(Auto.gas))
            print("\nIntegridad de las ruedas")
            print("R1: {}%\nR2: {}%\nR3: {}%\nR4: {}%".format(Auto.r1,
                                                                Auto.r2,
                                                                Auto.r3,
                                                                Auto.r4))
            Velocidad(Auto.vel, Auto.gas)
            if Auto.vel > 0:
                Ruedas(Auto.r1, Auto.r2, Auto.r3, Auto.r4)
            tiempo = random.randint(1, 10)
            km = (tiempo * Auto.vel) + km
            QuemarGas = Auto.vel / div
            burn = Actualizar(Auto.vel, Auto.gas - QuemarGas)
            Auto.GasolinaQuemada(burn)
        elif power is False:
            while power is False:
                print("\nPresione E para iniciar el motor")
                Nyom = input(">")
                if Nyom.upper() == "E":
                    print("\nBroom broom\n")
                    Paro = Actualizar(0, Auto.gas)
                    Auto.VelocidadCambio(Paro)
                    burn = Actualizar(Auto.vel, Auto.gas-1)
                    Auto.GasolinaQuemada(burn)
                    power = True
                else:
                    print("Comando incorrecto")
    print("\nSe acabo la gasolina")
    print("Usted recorrio {} km en total".format(km))
