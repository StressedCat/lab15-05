#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Cambios import Actualizar
from Ruedas import Desgastar


class CarDef():
    """Estadisticas basicas del auto"""
    def __init__(self, motor, gas, vel, r1, r2, r3, r4):
        self.motor = motor
        self.gas = gas
        self.vel = vel
        self.r1 = r1
        self.r2 = r2
        self.r3 = r3
        self.r4 = r4

    def VelocidadCambio(self, Fast):
        """Se cambia la velocidad"""
        self.vel = Fast.Zoom

    def GasolinaQuemada(self, Co2):
        """Se cambia el nivel de la gasolina"""
        self.gas = Co2.Gasofa

    def DesgasteRuedas(self, DR1, DR2, DR3, DR4):
        """Se cambia la integridad de las 4 ruedas"""
        self.r1 = DR1.D1
        self.r2 = DR2.D2
        self.r3 = DR3.D3
        self.r4 = DR4.D4
